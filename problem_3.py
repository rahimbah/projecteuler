#!/usr/bin/python


x = 600851475143
#x = 99999
i = 2
prime = 1
old_prime = 0

while x > 1:
    if x % i == 0:
        x = x / i
        old_prime = prime
	prime = i
	i = i + old_prime
    else:
        i = i + 1

print prime

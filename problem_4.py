#!/usr/bin/python


# if a number is palindrome it is the exact same number when reversed.
def isPalindrome(number):
    i = number
    rev = 0
    if number < 10:
        return True
    while i > 0:
        dig = i % 10
        rev = rev * 10 + dig
        i = i / 10
    if rev == number:
        return True
    else:
        return False

#list of products of 3 digit numbers
products = []
for i in range(999,100,-1):
    for j in range(999,100,-1):
        products.append(i*j)

print products[0:5]

while len(products) > 0:
    if isPalindrome(max(products)):
        print max(products)
        exit()
    else:
        products.remove(max(products))


#print isPalindrom(4)            
